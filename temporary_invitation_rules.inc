<?php
/**
 * @file
 * Temporary Invitation - Invite guests for a limited timespan.
 * This file provides support for workflow-ng.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of hook_entity_into().
 */
function temporary_invitation_entity_info() {
  return array(
    'temporary_invitation' => array(),
    'temporary_invitation_with_passcode' => array(),
    'temporary_invitation_mail' => array(),
  );
}

/**
 * Implementation of hook_event_info().
 */
function temporary_invitation_event_info() {
  return array(
    'temporary_invitation_insert' => array(
      '#label' => t('Invited user gets invited by host user.'),
      '#module' => t('Temporary Invitation'),
      '#arguments' => array(
        'invitation' => array(
          '#entity' => 'temporary_invitation_with_passcode',
          '#label' => t('invitation properties')
        ),
        'mail' => array(
          '#entity' => 'temporary_invitation_mail',
          '#label' => t('invitation mail')
        ),
        'invited-user' => array(
          '#entity' => 'user',
          '#label' => t('invited user (the user who received the invitation)'),
          '#handler' => 'temporary_invitation_events_argument_invited_user',
        ),
        'host-user' => array(
          '#entity' => 'user',
          '#label' => t('host user (the user who sent the invitation)'),
          '#handler' => 'temporary_invitation_get_host_user',
        ),
      ),
    ),
    'temporary_invitation_login' => array(
      '#label' => t('A temporarily invited user has logged in using a login code.'),
      '#module' => t('Temporary Invitation'),
      '#arguments' => array(
        'invitation' => array(
          '#entity' => 'temporary_invitation_with_passcode',
          '#label' => t('invitation properties.')
        ),
        'invited-user' => array(
          '#entity' => 'user',
          '#label' => t('invited user (the user who received the invitation)'),
          '#handler' => 'temporary_invitation_events_argument_invited_user',
        ),
        'host-user' => array(
          '#entity' => 'user',
          '#label' => t('host user (the user who sent the invitation)'),
          '#handler' => 'temporary_invitation_get_host_user',
        ),
      ),
    ),
    'temporary_invitation_delete' => array(
      '#label' => t('Invitation has been deleted.'),
      '#module' => t('Temporary Invitation'),
      '#arguments' => array(
        'invitation' => array(
          '#entity' => 'temporary_invitation',
          '#label' => t('invitation properties')
        ),
        'invited-user' => array(
          '#entity' => 'user',
          '#label' => t('invited user (the user who received the invitation)'),
          '#handler' => 'temporary_invitation_events_argument_invited_user',
        ),
        'host-user' => array(
          '#entity' => 'user',
          '#label' => t('host user (the user who sent the invitation)'),
          '#handler' => 'temporary_invitation_get_host_user',
        ),
      ),
    ),
  );
}

function temporary_invitation_events_argument_invited_user($invitation) {
  $invited_user = temporary_invitation_get_invited_user($invitation);

  // Provide the correct mail address for Token replacements,
  // hoping that the admin doesn't create a "save user" rule.
  $invited_user->mail = $invited_user->init;
  return $invited_user;
}
