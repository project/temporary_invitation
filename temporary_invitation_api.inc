<?php
/**
 * @file
 * Temporary Invitation - Invite guests for a limited timespan.
 * This file contains the "reusable" parts of the Temporary Invitation module.
 *
 * Copyright 2007 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 * Copyright 2006 by D R Pratten <http://www.davidpratten.com>
 */


/**
 * Invokes hook_temporary_invitation() for all modules that implemented it.
 *
 * @param $op
 *   What kind of action is being performed. Possible values:
 *   - 'insert': The invitation has been created and is readily accessible.
 *   - 'login':  The user associated with the invitation has just logged in
 *               by using the login ticket.
 *   - 'delete': The invitation is about to be deleted from the database, either
 *               because it has expired or because the host user has been
 *               deleted.
 * @param $invitation
 *   An invitation object, as returned by temporary_invitation_load().
 * @param $passcode
 *   If $op == 'insert' and $op == 'login', this contains the passcode
 *   as plaintext. For all other hook invocations this is NULL.
 * @param $mail
 *   If $op == 'insert', this contains a object with the attributes
 *   'to' (mail address), 'subject' (string), 'body' (string) and
 *   'copy_to_self' (boolean). This mail will be sent out to the invited user
 *   and (if 'copy_to_self' is TRUE) to the host user as a BCC copy.
 *   For all other hook invocations this is NULL.
 */
function temporary_invitation_invoke_all($op, $invitation, $passcode = NULL, $mail = NULL) {
  module_invoke_all('temporary_invitation', $op, $invitation, $passcode, $mail);
}


// array_fill_keys() (which is used in here) is only available in PHP 5.2
// or higher, so define it here for previous versions. This implementation
// depends on array_combine() which still requires PHP 5 or higher,
// so define that one as well.

if (!function_exists('array_combine')) {
  function array_combine($array1, $array2)
  {
    for ($i = 0; $i < count($array1); $i++) {
      $return_array[$array1[$i]] = $array2[$i];
    }
    if (isset($return_array)) {
      return $return_array;
    }
    return FALSE;
  }
}

if (!function_exists('array_fill_keys')) {
  function array_fill_keys($keys, $value) {
    if (count($keys) == 0) {
      return array();
    }
    return array_combine($keys, array_fill(0, count($keys), $value));
  }
}



/**
 * Retrieve all invitation objects for a given host user.
 *
 * @param $host_uid
 *   User ID of the user whose invitations shall be retrieved.
 * @param $include_expired
 *   If TRUE, this function also considers invitations that are already expired.
 *   Otherwise (by default), only valid invitations are included in the result.
 *
 * @returns  An array of invitations for the host user. The invitations
 *           themselves are objects with the attributes 'host_uid',
 *           'invited_uid', 'name', 'created' and 'ticket' (where 'ticket' is a
 *           complete ticket object as returned by loginticket_load()).
 */
function temporary_invitation_load_array($host_uid, $include_expired = FALSE) {
  $result = db_query('SELECT * FROM {temporary_invitation}
                      WHERE host_uid = %d
                      ORDER BY created DESC', $host_uid);
  $invitations = array();

  while ($invitation = db_fetch_object($result)) {
    $ticket = loginticket_load('temporary_invitation',
      array('passcode_md5' => $invitation->passcode_md5), $include_expired
    );
    if ($ticket !== FALSE) {
      $invitation->ticket = $ticket;
      $invitation->invited_uid = $ticket->uid;
      $invitations[] = $invitation;
    }
  }
  return $invitations;
}

/**
 * Retrieve the invitation object for a given passcode.
 *
 * @param $passcode_md5
 *   The MD5 hash of the invitation's passcode.
 * @param $include_expired
 *   If TRUE, this function also considers invitations that are already expired.
 *   Otherwise (by default), only a valid invitation is included in the result.
 *
 * @returns  An invitation object with attributes named 'host_uid',
 *           'invited_uid', 'name', 'created' and 'ticket' (where 'ticket' is a
 *           complete ticket object as returned by loginticket_load()).
 */
function temporary_invitation_load($passcode_md5, $include_expired = FALSE) {
  $result = db_query("SELECT * FROM {temporary_invitation}
                      WHERE passcode_md5 = '%s'", $passcode_md5);

  while ($invitation = db_fetch_object($result)) {
    $ticket = loginticket_load('temporary_invitation',
      array('passcode_md5' => $invitation->passcode_md5), $include_expired
    );
    if ($ticket !== FALSE) {
      $invitation->ticket = $ticket;
      $invitation->invited_uid = $ticket->uid;
      return $invitation;
    }
  }
  return FALSE;
}

/**
 * Create a temporary invitation, that is, create a new user and relate this
 * user with the given host user, and then create a login ticket that
 * the generated user can use to login.
 * When the ticket is created, hook_temporary_invitation($op='insert') is
 * called (which in turn is used by Temporary Invitation to notify the user
 * by sending login code and the link to the login form.
 *
 * @param $host_user
 *   The user that manages the invitation.
 * @param $invitation_name
 *   Short description for the invitation.
 * @param $mail
 *   An object with the attributes 'to' (mail address), 'subject' (string),
 *   'body' (string) and 'copy_to_self' (boolean). This mail will be sent out
 *   to the invited user and (if 'copy_to_self' is TRUE) to the host user
 *   as a BCC copy.
 * @param $expiration_time
 *   A Unix timestamp specifying when the invitation will expire.
 *
 * @return   An array, with the following values given for these keys:
 *   'invitation': An object containing the newly created invitation,
 *                 as returned by temporary_invitation_load().
 *   'passcode':   The passcode of the invitation as plaintext.
 */
function temporary_invitation_create($host_user, $invitation_name, $mail, $expiration_time) {
  $invited_user = temporary_invitation_create_user($host_user, $mail->to);
  $passcode = loginticket_create('temporary_invitation', $invited_user, $expiration_time);
  $passcode_md5 = md5($passcode);

  db_query("INSERT INTO {temporary_invitation}
            (passcode_md5, host_uid, name, created)
            VALUES ('%s', %d, '%s', %d)",
           $passcode_md5, $host_user->uid, $invitation_name, time());

  $invitation = temporary_invitation_load($passcode_md5);
  temporary_invitation_invoke_all('insert', $invitation, $passcode, $mail);

  return array(
    'invitation' => $invitation,
    'passcode' => $passcode,
  );
}

/**
 * Create a user with a random password and username.
 * If a user with the same mail address already exists, this user will instead
 * be suitably prepared so that she's able to log in with the passcode.
 *
 * @param $host_user
 *   The user object of the user who invites the guest.
 * @param $invited_user_mail
 *   The mail address of the invited user.
 *
 * @return  The user object as delivered by user_save().
 */
function temporary_invitation_create_user($host_user, $invited_user_mail) {
  $roles = array_fill_keys(_temporary_invitation_get_user_roles_default(), 1);

  if ($user = user_load(array('mail' => $invited_user_mail))) {
    // The user already exists, prepare that user to receive an invitation.
    $userinfo = array(
      'init' => $invited_user_mail,
      'status' => 1,
    );
    if (_temporary_invitation_existing_user_gets_roles()) {
      // array_merge() destroys the indexes, so we use "+" for merging
      $userinfo['roles'] = $user->roles + $roles;
    }
  }
  else {
    // The user doesn't exist yet, create a new one to invite.
    do {
      $suffix = isset($username) ? ('-'. user_password(4)) : '';
      $username = $invited_user_mail . $suffix;
      $user = user_load(array('name' => $username));
    } while ($user); // loop until we have a username that doesn't exist

    // Likewise, we need a unique email address.
    // We want to avoid that invited people can't create their own accounts
    // anymore, so instead of using the real mail address, we use fake one.
    // Not totally clean, but better than anything else I can think of.
    do {
      $fake_mail = 'temporary-user-'. user_password(8) .'@localhost';
      $user = user_load(array('mail' => $fake_mail));
    } while ($user); // loop until we have a mail address that doesn't exist

    $user = NULL;
    $userinfo = array(
      'name' => $username,
      'pass' => user_password(8),
      'mail' => $fake_mail,
      'init' => $invited_user_mail,
      'status' => 1,
      'roles' => $roles,
      // Only users with the following property will be blocked or deleted.
      // We really want to keep current users untouched.
      'temporary_invitation_remove_user' => TRUE,
    );
  }
  $user = user_save($user, $userinfo);

  watchdog('user',
    t('New temporarily invited user: %username.', array('%username' => $user->name)),
    WATCHDOG_NOTICE,
    l(t('edit'), 'user/'. $user->uid .'/edit')
  );
  return $user;
}

/**
 * Update all roles of invited users with a new set of roles.
 * In order to make sure no other roles are affected (in case the user
 * has other, unrelated roles as well) only those will be deleted that
 * have been selected before.
 *
 * @param $old_roles
 *   A plain list of old role ids for invited users.
 * @param $new_roles
 *   A plain list of new role ids for invited users.
 */
function temporary_invitation_update_user_roles($old_roles, $new_roles) {
  $new_roles = array_fill_keys($new_roles, 1);
  $tickets = loginticket_load_array('temporary_invitation', array(), TRUE);

  foreach ($tickets as $ticket) {
    $user = user_load(array('uid' => $ticket->uid));

    if ($user !== FALSE) {
      if ($user->temporary_invitation_remove_user /* we created that user */
          || _temporary_invitation_existing_user_gets_roles()) {
        $roles = $user->roles + $new_roles; // array_merge() destroys the indexes
        foreach ($old_roles as $rid) {
          if (!isset($new_roles[$rid])) {
            unset($roles[$rid]);
          }
        }
        user_save($user, array('roles' => $roles));
      }
    }
  }
}

/**
 * Delete the invitation for the given user that had been invited, and call
 * hook_temporary_invitation($op='delete', $invitation, NULL) before that.
 *
 * @param $passcode_md5
 *   The MD5 hash of the passcode for the invitation that should be deleted
 * @param $redirect
 *   If TRUE, drupal_goto() is called, which uses the 'destination' parameter
 *   for redirecting to the page that is given in the delete link.
 *   If FALSE, drupal_goto() is not called, and you can continue execution.
 */
function temporary_invitation_delete($passcode_md5, $redirect = FALSE) {
  global $user;
  $invitation = temporary_invitation_load($passcode_md5, TRUE);

  // safety check: only the user possessing the invitation may delete it
  if ($invitation !== FALSE
      && (($user->uid == $invitation->host_uid && user_access('manage own temporary invitations'))
          || user_access('manage temporary invitations')))
  {
    loginticket_delete($invitation->ticket);

    if ($redirect) {
      drupal_goto();
    }
  }
  else {
    watchdog('user',
      t('Detected malicious attempt to delete an invitation.'),
      WATCHDOG_WARNING,
      l(t('view'), 'user/'. $user->uid)
    );
    if ($redirect) {
      drupal_access_denied();
    }
  }
}


/**
 * Retrieve the invited user for a given invitation.
 * Note that the mail address doesn't generally contain the address that was
 * given for the invitation mail, as this would prevent that user from
 * registering a regular account on this site. Instead, the 'init' property
 * of the user object contains the invited mail address.
 */
function temporary_invitation_get_invited_user($invitation) {
  global $user;

  if ($invitation->invited_uid == $user->uid) {
    return drupal_clone($user);
  }
  else {
    return user_load(array('uid' => $invitation->invited_uid));
  }
}

/**
 * Retrieve the host user for a given invitation.
 */
function temporary_invitation_get_host_user($invitation) {
  global $user;

  if ($invitation->host_uid == $user->uid) {
    return drupal_clone($user);
  }
  else {
    return user_load(array('uid' => $invitation->host_uid));
  }
}



/**
 * Implementation of hook_user():
 * If a host user is deleted, also do away with the associated invitations.
 * hook_temporary_invitation($op='delete', $invitation, NULL) is called
 * for each deleted invitation, before the delete action is performed.
 */
function temporary_invitation_user($op, &$edit, &$account, $category = NULL) {
  if ($op == 'delete') {
    $invitations = temporary_invitation_load_array($account->uid, TRUE);
    $tickets = array();

    foreach ($invitations as $invitation) {
      $tickets[] = $invitation->ticket;
    }
    // let loginticket_delete() invoke all the cleanup operations by itself
    loginticket_delete($tickets);
  }
}

/**
 * Implementation of hook_loginticket():
 * When a ticket is deleted, also delete the associated user
 * and invitation entry from the database, and call
 * hook_temporary_invitation($op='delete', $invitation, NULL)
 * before performing the actual delete actions.
 */
function temporary_invitation_loginticket($op, $ticket, $passcode) {
  if ($ticket->purpose != 'temporary_invitation') {
    return;
  }

  switch ($op) {
    case 'delete':
      $invitation = temporary_invitation_load($ticket->passcode_md5, TRUE);
      if ($invitation !== FALSE) {
        temporary_invitation_invoke_all('delete', $invitation, NULL, NULL);
      }

      // Delete the invitation entry.
      db_query("DELETE FROM {temporary_invitation} WHERE passcode_md5 = '%s'",
               $ticket->passcode_md5);

      // Delete the user if (s)he still exists at all.
      $user = user_load(array('uid' => $ticket->uid));

      if ($user && $user->temporary_invitation_remove_user) {
        // Check if any other valid invitations remain for this user.
        // After all, we don't want to remove him when another invitation
        // is still available.
        $remaining_tickets = loginticket_load_array(
          'temporary_invitation', array('uid' => $ticket->uid)
        );
        foreach ($remaining_tickets as $key => $remaining_ticket) {
          if ($remaining_ticket->passcode_md5 == $ticket->passcode_md5) {
            unset($remaining_tickets[$key]); // ignore the just deleted one.
          }
        }

        if (empty($remaining_tickets)) {
          // No more invitations left for this user: get rid of the sucker.
          switch (_temporary_invitation_get_delete_action()) {
            case 'block':
              if ($user->status == 1) {
                user_save($user, array('status' => 0));
              }
              break;

            case 'delete':
              // Workaround to disable the "<username> has been deleted." message.
              $messages = drupal_get_messages();

              user_delete(array(), $ticket->uid);

              // Write back the old messages.
              $_SESSION['messages'] = $messages;
              break;
          }
        }
      }
      return;

    case 'login':
      $invitation = temporary_invitation_load($ticket->passcode_md5);
      if ($invitation !== FALSE) {
        temporary_invitation_invoke_all('login', $invitation, $passcode, NULL);
      }

    default:
      return;
  }
}



/**
 * Implementation of hook_elements():
 * Register the duration widget with the Forms API and set default values.
 */
function temporary_invitation_elements() {
  $type['duration'] = array(
    // the $form_values elements will be named '#name'_metric and '#name'_value
    '#name' => 'duration',
    '#default_metric' => 'mon',
    '#default_value' => 1,
    '#process' => array('_temporary_invitation_duration_process' => array()),
  );
  return $type;
}

/**
 * The process hook for 'duration' type widgets.
 * Called after defining the form and while building it.
 */
function _temporary_invitation_duration_process($element) {
  $name = $element['#name'];
  $element[$name .'_value'] = array(
    '#type' => 'textfield',
    '#default_value' => $element['#default_value'],
    '#size' => 3,
    '#maxlength' => 2,
    '#required' => TRUE,
  );
  $element[$name .'_metric'] = array(
    '#type' => 'select',
    '#default_value' => $element['#default_metric'],
    '#options' => _temporary_invitation_duration_metric_options(),
  );
  return $element;
}

function _temporary_invitation_duration_metric_options() {
  return array(
    'hours' => t('hours'),
    'mday' => t('days'),
    'mon' => t('months'),
  );
}

function theme_duration($element) {
  // class="container-inline" makes child widgets align horizontally
  return theme('form_element', $element,
    '<div class="container-inline">'. $element['#children']. '</div>'
  );
}

/**
 * Add the timespan of a given duration widget to a given point in time.
 *
 * @param $name
 *   The '#name' property of the duration widget.
 * @param $form_values
 *   The $form_values array of the returned form, as received in form_submit().
 * @param $timestamp
 *   The time to which the duration is added. Defaults to the current time.
 */
function temporary_invitation_duration_widget_add($name, $form_values, $timestamp = NULL) {
  return temporary_invitation_duration_add(
    $form_values[$name .'_metric'], $form_values[$name .'_value'], $timestamp
  );
}

/**
 * Add a timespan to a given point in time.
 *
 * @param $metric
 *   What kind of value should be added to the timestamp. This may be one of
 *   'hours', 'minutes', 'seconds', 'mon', 'mday' or 'year', as used in the
 *   associative array returned by getdate().
 * @param $value
 *   How much of the time metric should be added.
 * @param $timestamp
 *   The time to which the duration is added. Defaults to the current time.
 */
function temporary_invitation_duration_add($metric, $value, $timestamp = NULL) {
  if (!isset($timestamp)) {
    $timestamp = time();
  }
  $date = getdate($timestamp);
  $date[$metric] += $value;

  return mktime(
    $date['hours'], $date['minutes'], $date['seconds'],
    $date['mon'], $date['mday'], $date['year']
  );
}

/**
 * Subtract the timespan of a given duration widget from a given point in time.
 *
 * @param $name
 *   The '#name' property of the duration widget.
 * @param $form_values
 *   The $form_values array of the returned form, as received in form_submit().
 * @param $timestamp
 *   The time from which the duration is subtracted.
 *   Defaults to the current time.
 */
function temporary_invitation_duration_widget_subtract($name, $form_values, $timestamp = NULL) {
  return temporary_invitation_duration_subtract(
    $form_values[$name .'_metric'], $form_values[$name .'_value'], $timestamp
  );
}

/**
 * Subtract a timespan to a given point in time.
 *
 * @param $metric
 *   What kind of value should be subtracted from the timestamp. This may be
 *   one of 'hours', 'minutes', 'seconds', 'mon', 'mday' or 'year', as used
 *   in the associative array returned by getdate().
 * @param $value
 *   How much of the time metric should be subtracted.
 * @param $timestamp
 *   The time from which the duration is subtracted.
 *   Defaults to the current time.
 */
function temporary_invitation_duration_subtract($metric, $value, $timestamp = NULL) {
  if (!isset($timestamp)) {
    $timestamp = time();
  }
  temporary_invitation_duration_add($metric, $value * -1, $timestamp);
}



/**
 * Retrieve the roles that will be assigned to newly invited users.
 */
function _temporary_invitation_get_user_roles_default() {
  return variable_get('temporary_invitation_user_roles_default', array());
}

/**
 * Retrieve the roles that will be assigned to newly invited users.
 */
function _temporary_invitation_existing_user_gets_roles() {
  return !variable_get('temporary_invitation_no_roles_for_existing_user', TRUE);
}

/**
 * Find out what shall be done with the user belonging to an invitation
 * if the invitation is deleted.
 *
 * @param $which
 *   If set to 'all', an associative array consisting of all available
 *   delete action keys and descriptions is returned.
 *   If set to 'selected', the function returns the currently selected key,
 *   which is either 'delete' or 'block'.
 */
function _temporary_invitation_get_delete_action($which = 'selected') {
  if ($which == 'selected') {
    return variable_get('temporary_invitation_delete_action', 'delete');
  }
  else if ($which == 'all') {
    return array(
      'delete' => t('Delete the associated user'),
      'block' => t('Block the associated user'),
    );
  }
}
