
Temporary Invitation -
Invite guests for a limited timespan.


SHORT DESCRIPTION
-----------------
This module enables site users to invite guests for a limited timespan.
For each invitation, a new user is created, together with login code
(e.g. "EbN9F6") that the user can use to log in. When the invitation expires,
the associated user is either blocked or deleted, so the invited user can't
log in anymore.

As opposed to the Invite module, Temporary Invitation focuses on restricted
invitations, like guest accounts with extra limitations. Use the Invite module
if you want to invite friends that are supposed to stay on the site for a long
time and gain permissions similar to the ones of the inviting user.

Features:
* Admin-defined set of user roles that is assigned to invited users
* Notification by e-mail, authored by the host user
* Customizable default time until expiration
* Customizable path where an invited user is redirected to after logging in

Temporary Invitation depends on the Login Ticket API module.


AUTHOR/MAINTAINER
-----------------
Jakob Petsovits <jpetso at gmx DOT at>


CREDITS
-------
Some code in Login Ticket was taken from the Site Pass module for Drupal 5.x
which was written by D R Pratten <http://www.davidpratten.com>.
